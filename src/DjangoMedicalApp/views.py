# django_restframework
from rest_framework import viewsets, generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

# restframework-simplejwt
from rest_framework_simplejwt.authentication import JWTAuthentication

# project apps
from . models import Company, CompanyBank, Medicine
from . serializers import (
    CompanySerializer,
    CompanyBankSerializer,
    MedicineSerializer
)


class CompanyViewSet(viewsets.ViewSet):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def list(self, request):
        company = Company.objects.all()
        serializer = CompanySerializer(
            company, many=True, context={'request': request})
        response_dict = {
            'error': False,
            'message': 'All Company List Data',
            'data': serializer.data,
        }
        return Response(response_dict)

    def create(self, request):
        company_data = request.data
        serializer = CompanySerializer(
            data=company_data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            response_dict = {
                'error': False,
                'message': 'Data successfully submited',
            }
        else:
            response_dict = {
                'error': True,
                'message': serializer.errors,
            }

        return Response(response_dict)

    def retrieve(self, request, pk=None):
        try:
            if pk is not None:
                company = Company.objects.get(id=pk)
            if company:
                serializer = CompanySerializer(
                    company, context={'request': request})

                serializer_data = serializer.data

                companyBankList = CompanyBank.objects.filter(
                    company_id=serializer_data['id'])

                company_bank_serializer = CompanyBankSerializer(
                    companyBankList, many=True)
                serializer_data['companyBankList'] = company_bank_serializer.data
                response_dict = {
                    'error': False,
                    'message': 'Data has been retrieved.',
                    'data': serializer_data,
                }
        except Exception as e:
            print(e)
            response_dict = {
                'error': True,
                'message': str(e)
            }
        return Response(response_dict)

    def update(self, request, pk=None):
        if pk is not None:
            company = Company.objects.get(id=pk)
        if company:
            serializer = CompanySerializer(
                company, data=request.data, context={'request': request})
            if serializer.is_valid():
                serializer.save()
                response_dict = {
                    'error': False,
                    'message': 'Data successfully submited',
                }
            else:
                response_dict = {
                    'error': True,
                    'message': serializer.errors,
                }

        return Response(response_dict)
# company_list = CompanyViewSet.as_view({"get": 'list'})
# company_create = CompanyViewSet.as_view({"post": 'create'})
# company_update = CompanyViewSet.as_view({"put": 'update'})


class CompanyBankViewSet(viewsets.ViewSet):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def list(self, request):
        company_bank = CompanyBank.objects.all()
        serializer = CompanyBankSerializer(
            company_bank, many=True, context={'request': request})
        response_dict = {
            'error': False,
            'message': 'All Company Bank List Data',
            'data': serializer.data,
        }
        return Response(response_dict)

    def create(self, request):
        company_data = request.data
        serializer = CompanyBankSerializer(
            data=company_data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            response_dict = {
                'error': False,
                'message': 'Data successfully submited',
            }
        else:
            response_dict = {
                'error': True,
                'message': serializer.errors,
            }

        return Response(response_dict)

    def retrieve(self, request, pk=None):
        try:
            if pk is not None:
                companybank = CompanyBank.objects.get(id=pk)
            if companybank:
                serializer = CompanyBankSerializer(
                    companybank,  context={'request': request})
                response_dict = {
                    'error': False,
                    'message': 'Data successfully submited',
                    'data': serializer.data
                }
        except Exception as e:
            print(type(e))
            response_dict = {
                'error': True,
                'message': str(e),
            }

        return Response(response_dict)

    def update(self, request, pk=None):
        if pk is not None:
            companybank = CompanyBank.objects.get(id=pk)
        if companybank:
            serializer = CompanySerializer(
                companybank, data=request.data, context={'request': request})
            if serializer.is_valid():
                serializer.save()
                response_dict = {
                    'error': False,
                    'message': 'Data has been successfully updated',
                }
            else:
                response_dict = {
                    'error': True,
                    'message': serializer.errors,
                }

        return Response(response_dict)


class CompanyNameViewSet(generics.ListAPIView):
    serializer_class = CompanySerializer

    def get_queryset(self):
        name = self.kwargs['name']
        return Company.objects.filter(name=name)


class MedicineViewSet(viewsets.ViewSet):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def list(self, request):
        medicine = Medicine.objects.all()
        serializer = MedicineSerializer(
            medicine, many=True, context={'request': request})
        response_dict = {
            'error': False,
            'message': 'All Medicine List Data',
            'data': serializer.data,
        }
        return Response(response_dict)

    def create(self, request):
        medicine_data = request.data
        serializer = MedicineSerializer(
            data=medicine_data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            response_dict = {
                'error': False,
                'message': 'Medicine Data successfully submited',
            }
        else:
            response_dict = {
                'error': True,
                'message': serializer.errors,
            }

        return Response(response_dict)

    def retrieve(self, request, pk=None):
        try:
            if pk is not None:
                medicine = Medicine.objects.get(id=pk)
            if medicine:
                serializer = MedicineSerializer(
                    medicine,  context={'request': request})
                response_dict = {
                    'error': False,
                    'message': 'Medicine data successfully has been retrieved',
                    'data': serializer.data
                }
        except Exception as e:
            print(type(e))
            response_dict = {
                'error': True,
                'message': str(e),
            }

        return Response(response_dict)

    def update(self, request, pk=None):
        if pk is not None:
            medicine = CompanyBank.objects.get(id=pk)
        if medicine:
            serializer = MedicineSerializer(
                medicine, data=request.data, context={'request': request})
            if serializer.is_valid():
                serializer.save()
                response_dict = {
                    'error': False,
                    'message': 'Data has been successfully updated',
                }
            else:
                response_dict = {
                    'error': True,
                    'message': serializer.errors,
                }

        return Response(response_dict)
