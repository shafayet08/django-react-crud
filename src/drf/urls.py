
from django.contrib import admin
from django.urls import path, include

from rest_framework_simplejwt.views import(
        TokenObtainPairView, 
        TokenRefreshView
    )

from rest_framework import routers
from DjangoMedicalApp import views 


router = routers.DefaultRouter()
router.register('company', views.CompanyViewSet, basename='company')
router.register('companybank', views.CompanyBankViewSet, basename='companybank')
router.register('medicine', views.MedicineViewSet, basename='medicine')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api/gettoken/', TokenObtainPairView.as_view(), name='gettoken'),
    path('api/refresh_token/', TokenRefreshView.as_view(), name='refresh_token'),
    path('api/companybyname/<str:name>/', views.CompanyNameViewSet.as_view(), name='companybyname')
    # path('', include('DjangoMedicalApp.urls', namespace="DjangoMedicalApp"))
]

from django.conf import settings
from django.conf.urls.static import static

if settings.DEBUG:
    urlpatterns +=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
