from django.contrib import admin
from django.conf import settings
from users.models import User
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from django.forms import TextInput, Textarea
from django.contrib.auth.forms import AdminPasswordChangeForm

# Register your models here.
class UserAdminConfig(UserAdmin):
    ordering = (('-date_joined'),)
    list_display = ('email', 'username', 'is_active', 'is_staff', 'is_manager', 'is_salesman', 'is_owner', 'is_superuser'
            )
    fieldsets = (
        (None, {'fields': ('email', 'username', 'password')}),
        ('Personal', {'fields': ('first_name', 'last_name', 'contact', 'location', 'bio',)}),
        ('Groups and User Permissions', {'fields': ('groups', 'user_permissions',)}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_salesman', 'is_manager', 'is_owner', 'is_superuser',)}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    formfield_overrides = {
        User.bio: {'widget': Textarea(attrs={'rows': 10, 'cols': 40})},
    }
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'password1', 'password2', 'is_salesman', 'is_manager', 'is_owner',)
        }),
    )

admin.site.register(User, UserAdminConfig)

# admin.site.register(User)