# Generated by Django 3.1.7 on 2021-07-30 11:03

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20210730_1059'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='auth_token',
            field=models.CharField(default=uuid.UUID('fc253927-4f98-422d-b8ea-f34ffcdcf50a'), max_length=100),
        ),
    ]
